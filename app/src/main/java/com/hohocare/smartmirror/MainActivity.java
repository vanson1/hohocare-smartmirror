package com.hohocare.smartmirror;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.hohocare.smartmirror.helpers.DateTimeHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private final BroadcastReceiver clockChangedReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            updateView();
        }
    };
    @BindView(R.id.bg)
    ImageView bg;
    @BindView(R.id.ttime)
    TextView ttime;
    @BindView(R.id.tdate)
    TextView tdate;
    @BindView(R.id.room_temp)
    TextView roomTemp;
    @BindView(R.id.room_hum)
    TextView roomHum;
    @BindView(R.id.outdoor_temp)
    TextView outdoorTemp;
    @BindView(R.id.outdoor_hum)
    TextView outdoorHum;
    @BindView(R.id.notification_ball)
    RelativeLayout notificationBall;
    @BindView(R.id.notification_alarm)
    RelativeLayout notificationAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        registerReceiver(clockChangedReceiver, intentFilter);
        EventBus.getDefault().register(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(clockChangedReceiver);
        EventBus.getDefault().unregister(this);
    }


    void updateView() {
        Calendar c = Calendar.getInstance();
        int ihours = c.get(Calendar.HOUR);
        int imins = c.get(Calendar.MINUTE);
        int iam_pm = c.get(Calendar.AM_PM);
        int iseconds = c.get(Calendar.SECOND);

        String sampm = "AM";
        if (iam_pm == Calendar.PM) {
            sampm = "PM";
        }

        if (ihours == 0) {
            if (sampm.equals("PM")) {
                ihours = 12;
            }
        }

        Date date = new Date();

        tdate.setText(DateTimeHelper.datetimeWeekRepresentation(date));

        ttime.setText(String.format("%d:%02d%s", ihours, imins, sampm));

    }
}
