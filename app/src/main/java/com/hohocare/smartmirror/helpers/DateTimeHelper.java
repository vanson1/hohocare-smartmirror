package com.hohocare.smartmirror.helpers;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeHelper {

    public static Locale getLocale() {
        return Locale.getDefault();
    }

    public static String weekLongRepresentation(Date date)
    {
        SimpleDateFormat dateFormatWeek = new SimpleDateFormat("EEEE", getLocale());
        String week = dateFormatWeek.format(date).toUpperCase();

        return String.format("%s", week);
    }

    public static String datetimeLongRepresentation(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM yyyy", getLocale());
        String formatted = dateFormat.format(new Date());

        return String.format("%s", formatted);
    }

    public static String datetimeWeekRepresentation(Date date)
    {
        SimpleDateFormat dateFormatWeek = new SimpleDateFormat("E", getLocale());
        String week = dateFormatWeek.format(date).toUpperCase();

        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy", getLocale());
        String formatted = dateFormat.format(new Date());

        return String.format("%s %s", formatted, week);
    }


}
